-- app_food - Node26 Trần Quốc Việt

-- Tạo và sử dụng database
DROP DATABASE if EXISTS app_food;
CREATE DATABASE app_food;
USE app_food;

-- * Query tạo và tương tác dữ liệu với Table *

-- Table users

DROP TABLE IF EXISTS users;
CREATE TABLE users(
   id INT PRIMARY KEY AUTO_INCREMENT,
   full_name VARCHAR(255),
   email VARCHAR(255),
   password VARCHAR(20)
);

DELETE FROM users;
ALTER TABLE users AUTO_INCREMENT = 1;

INSERT INTO users(full_name, email, password)
VALUES
   ('Tran Van A', 'tva@g.co', 'a123'),
   ('Tran Van B', 'tvb@g.co', 'a123'),
   ('Tran Van C', 'tvc@g.co', 'a123'),
   ('Tran Van D', 'tvd@g.co', 'a123'),
   ('Tran Van E', 'tve@g.co', 'a123'),
   ('Tran Van F', 'tvf@g.co', 'a123'),
   ('Tran Van G', 'tvg@g.co', 'a123'),
   ('Tran Van H', 'tvh@g.co', 'a123'),
   ('Tran Van I', 'tvi@g.co', 'a123'),
   ('Tran Van G', 'tvj@g.co', 'a123'),
   ('Tran Van K', 'tvk@g.co', 'a123'),
   ('Tran Van L', 'tvl@g.co', 'a123'),
   ('Tran Van M', 'tvm@g.co', 'a123'),
   ('Tran Van N', 'tvn@g.co', 'a123'),
   ('Tran Van O', 'tvo@g.co', 'a123'),
   ('Tran Van P', 'tvp@g.co', 'a123'),
   ('Tran Van Q', 'tvq@g.co', 'a123'),
   ('Tran Van R', 'tvr@g.co', 'a123'),
   ('Tran Van S', 'tvs@g.co', 'a123'),
   ('Tran Van T', 'tvt@g.co', 'a123') ;


-- Table restaurants

DROP TABLE IF EXISTS restaurants;
CREATE TABLE restaurants(
   id INT PRIMARY KEY AUTO_INCREMENT,
   name VARCHAR(255),
   image VARCHAR(255),
   description VARCHAR(255)
);

DELETE FROM restaurants;
ALTER TABLE restaurants AUTO_INCREMENT = 1;

INSERT INTO restaurants(name, image, description)
VALUES
   ('KFC', 'https://static.kfcvietnam.com.vn/images/category/lg/COMBO%201%20NGUOI.jpg?v=E3QQZ3', 'Kentucky Fried Chicken'),
   ('Lotteria', 'http://smart-supermarket.com.vn/wp-content/uploads/2020/06/a5-e1593482773289.jpg', 'Fried Chicken from Korea'),
   ('Burger King', 'https://images.foody.vn/res/g3/26916/prof/s640x400/foody-upload-api-foody-mobile-sh-b511f740-221001081535.jpeg', 'King of Hamburger'),
   ('MAYCHA', 'https://images.foody.vn/res/g74/739438/prof/s1242x600/foody-upload-api-foody-mobile-2a-200504093905.jpg', 'Tà tữa của May'),
   ('KOI', 'https://aeonmall-hadong.com.vn/wp-content/uploads/2020/05/bubble-milk-tea.jpg', 'Koi tà tữa thử Koi')  ;


-- Table food type

DROP TABLE IF EXISTS food_types;
CREATE TABLE food_types(
   id INT PRIMARY KEY AUTO_INCREMENT,
   name VARCHAR(255)
);

DELETE FROM food_types;
ALTER TABLE food_types AUTO_INCREMENT = 1;

INSERT INTO food_types(name)
VALUES
   ('Fried chicken'),
   ('Burger'),
   ('Fried potato'),
   ('Tea'),
   ('Milk'),
   ('Ice cream')  ;

-- Table food

DROP TABLE IF EXISTS food;
CREATE TABLE food(
   id INT PRIMARY KEY AUTO_INCREMENT,
   name VARCHAR(255),
   image VARCHAR(255),
   price DECIMAL(11,2),
   description VARCHAR(255),
   type_id INT,
   FOREIGN KEY (type_id) REFERENCES food_types(id) ON UPDATE CASCADE ON DELETE CASCADE
);

DELETE FROM food;
ALTER TABLE food auto_increment = 1;

INSERT INTO food(name, image, price, description, type_id)
VALUES
   ('Fried Chicken 1', 'https://static.kfcvietnam.com.vn/images/category/lg/COMBO%201%20NGUOI.jpg?v=E3QQZ3', 77000, 'Fresh chicken wings', 1 ),
   ('Fried Chicken 2', 'https://c.ndtvimg.com/2020-08/2dv9fku_kfc-covid_625x300_25_August_20.jpg', 65000, 'Fresh chicken legs', 1 ),
   ('Hamburger 1', 'https://media-cdn.tripadvisor.com/media/photo-s/17/3b/9a/d2/burger-king.jpg', 53000, 'Burger with beef', 2 ),
   ('Hamburger 2', 'https://images.bolt.eu/store/2022/2022-09-01/c2098142-363c-48d9-a541-51586e14b703.jpeg', 46000, 'Burger with pork', 2 ),
   ('French Fried', 'https://i.insider.com/564a5db0112314303e8b577d?width=1136&format=jpeg', 33000, 'Fresh potato', 3 ),
   ('Bubble Tea', 'https://cdn.vox-cdn.com/thumbor/BLEQSyZC9voUX7wpIkAiZiETAy0=/0x0:3625x3625/1200x0/filters:focal(0x0:3625x3625):no_upscale()/cdn.vox-cdn.com/uploads/chorus_asset/file/14548425/SML_190218_1054.jpg', 69000, 'Bubble tea Hong Kong style', 4 ),
   ('Milk With Boba', 'http://gourmetvegetariankitchen.com/wp-content/uploads/2018/04/bubble-tea-without-tea.jpg', 66000, 'Fresh milk and black sugar boba', 5 ),
   ('Ice Cream', 'https://www.koshericecream.com/wp-content/uploads/van-choc-swirl.png', 5000, 'Fresh milk ice cream', 6 )
   ;

-- Table sub_food

DROP TABLE IF EXISTS sub_food;
CREATE TABLE sub_food(
   id INT PRIMARY KEY AUTO_INCREMENT,
   name VARCHAR(255),
   price DEC(7,2),
   food_id INT,
   FOREIGN KEY (food_id) REFERENCES food(id) ON UPDATE CASCADE ON DELETE CASCADE
);

DELETE FROM sub_food;
ALTER TABLE sub_food AUTO_INCREMENT = 1;

INSERT INTO sub_food(name, price, food_id)
VALUES
   ('Tomato', 3000, 1),
   ('Salad', 4000, 1),
   ('Pepsi', 2000, 2),
   ('Coke', 2500, 2),
   ('7up', 3000, 2),
   ('Fresh Water', 1000, 3),
   ('Butter', 1500, 3),
   ('Mayonaise', 700, 3),
   ('Cucumber', 3000, 4),
   ('Ketchup', 500, 5),
   ('Jello', 3000, 6),
   ('Flan', 3500, 6),
   ('Boba', 2000, 6),
   ('Sugar', 400, 6),
   ('Non-fat Milk', 6000, 7),
   ('Soya Milk', 5500, 7),
   ('Chocolate Chip', 800, 8),
   ('Haribo', 900, 8),
   ('Condensed Milk', 600, 8)   ;


-- Table orders

DROP TABLE IF EXISTS orders;
CREATE TABLE orders(
   user_id INT,
   FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
   food_id INT,
   FOREIGN KEY (food_id) REFERENCES food(id) ON UPDATE CASCADE ON DELETE CASCADE,
   amount INT,
   code VARCHAR(255),
   arr_sub_id VARCHAR (255),
   date_order DATETIME
);

DELETE FROM orders;

INSERT INTO orders(user_id, food_id, amount, arr_sub_id, code, date_order)
VALUES
   (1, 2, 1, '3, 4', '', '2022-02-02 22-22-22'),
   (4, 5, 4, '10', '', '2022-02-03 20-24-52'),
   (6, 5, 2, '10', 'DISCOUNT20', '2022-02-04 12-00-24'),
   (11, 1, 3, '1, 2', '', '2022-02-06 09-21-15'),
   (16, 8, 5, '17, 19', '', '2022-02-06 10-22-42'),
   (7, 6, 2, '12, 13', 'DISCOUNT40', '2022-02-06 13-44-53'),
   (3, 1, 4, '2', 'FREESHIP', '2022-02-07 05-25-32'),
   (12, 3, 2, '6', '', '2022-02-07 11-32-09'),
   (19, 7, 1, '16', '', '2022-02-08 08-28-52'),
   (17, 7, 6, '15, 16', 'FREESHIP', '2022-02-08 08-29-29'),
   (3, 8, 5, '19', '', '2022-02-08 09-24-33'),
   (5, 4, 2, '9', '', '2022-02-08 10-55-21'),
   (13, 7, 14, '15', 'DISCOUNT40', '2022-02-08 11-11-47'),
   (20, 1, 8, '2', '', '2022-02-08 12-12-31'),
   (15, 2, 5, '4', 'FREESHIP', '2022-02-08 14-52-52'),
   (4, 2, 2, '5', '', '2022-02-08 17-09-28'),
   (12, 8, 4, '18', 'DISCOUNT10', '2022-02-08 19-08-04'),
   (13, 6, 2, '13', '', '2022-02-09 09-08-07'),
   (1, 2, 3, '5', 'FREESHIP', '2022-02-10 17-29-42'),
   (13, 6, 5, '13', '', '2022-02-11 11-08-11')
   ;
   

-- Table rate_res

DROP TABLE IF EXISTS rate_res;
CREATE TABLE rate_res(
   user_id INT,
   FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
   res_id INT,
   FOREIGN KEY (res_id) REFERENCES restaurants(id) ON UPDATE CASCADE ON DELETE CASCADE,
   amount ENUM('1', '2', '3', '4', '5') DEFAULT '5',
   date_rate DATETIME
);

DELETE FROM rate_res;

INSERT INTO rate_res(user_id, res_id, amount, date_rate)
VALUES
   (1, 1, 4, '2022-02-02 22-22-22'),
   (4, 1, 3, '2022-02-03 20-24-52'),
   (6, 3, 5, '2022-02-04 12-00-24'),
   (11, 1, 3, '2022-02-06 09-21-15'),
   (16, 2, 5, '2022-02-06 10-22-42'),
   (7, 5, 3, '2022-02-06 13-44-53'),
   (3, 1, 1, '2022-02-07 05-25-32'),
   (12, 3, 5, '2022-02-07 11-32-09'),
   (19, 4, 4, '2022-02-08 08-28-52'),
   (17, 4, 1, '2022-02-08 08-29-29'),
   (3, 2, 5, '2022-02-08 09-24-33'),
   (5, 3, 2, '2022-02-08 10-55-21'),
   (13, 4, 1, '2022-02-08 11-11-47'),
   (20, 1, 1, '2022-02-08 12-12-31'),
   (15, 2, 5, '2022-02-08 14-52-52'),
   (4, 2, 2, '2022-02-08 17-09-28'),
   (12, 3, 4, '2022-02-08 19-08-04'),
   (13, 5, 2, '2022-02-09 09-08-07'),
   (1, 2, 5, '2022-02-10 17-29-42')
   ;


-- Table like_res

DROP TABLE IF EXISTS like_res;
CREATE TABLE like_res(
   user_id INT,
   FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
   res_id INT,
   FOREIGN KEY (res_id) REFERENCES restaurants(id) ON UPDATE CASCADE ON DELETE CASCADE,
   date_like DATETIME
);

DELETE FROM like_res;

INSERT INTO like_res(user_id, res_id, date_like)
VALUES
   (1, 1, '2022-02-02 22-22-22'),
   (4, 1, '2022-02-03 20-24-52'),
   (16, 3, '2022-02-04 12-00-24'),
   (11, 1, '2022-02-06 09-21-15'),
   (16, 2, '2022-02-06 10-22-42'),
   (4, 5, '2022-02-06 13-44-53'),
   (3, 1, '2022-02-07 05-25-32'),
   (12, 3, '2022-02-07 11-32-09'),
   (19, 4, '2022-02-08 08-28-52'),
   (17, 4, '2022-02-08 08-29-29'),
   (4, 2, '2022-02-08 09-24-33'),
   (5, 3, '2022-02-08 10-55-21'),
   (3, 4, '2022-02-08 11-11-47'),
   (20, 1, '2022-02-08 12-12-31'),
   (11, 2, '2022-02-08 14-52-52'),
   (4, 2, '2022-02-08 17-09-28'),
   (11, 3, '2022-02-08 19-08-04'),
   (10, 5, '2022-02-09 09-08-07'),
   (1, 3, '2022-02-10 17-29-42'),
   (16, 3, '2022-02-10 17-33-45')
   ;



-- * Query đọc dữ liệu từ Table *

-- Trả lời các câu hỏi trong đề bài
-- 1) Tìm 5 người đã like nhà hàng nhiều nhất

SELECT COUNT(users.id) as total_like_give, users.full_name, users.email
FROM users
INNER JOIN like_res
ON users.id = like_res.user_id
INNER JOIN restaurants
ON like_res.res_id = restaurants.id
GROUP BY users.id
ORDER BY total_like_give DESC
LIMIT 5
;

-- 2) Tìm 2 nhà hàng có lượt like nhiều nhất

SELECT COUNT(restaurants.id) as total_like_from_users, restaurants.name
FROM restaurants
INNER JOIN like_res
ON restaurants.id = like_res.res_id
GROUP BY restaurants.id
ORDER BY total_like_from_users DESC
LIMIT 2
;

-- 3) Tìm người đã đặt hàng nhiều nhất

-- a) Người đặt nhiều order nhất
SELECT COUNT(users.id) as total_order, users.full_name, users.email
FROM users
INNER JOIN orders
ON users.id = orders.user_id
GROUP BY users.id
ORDER BY total_order DESC
LIMIT 1
;

-- b) Người mua số lượng hàng nhiều nhất
SELECT SUM(orders.amount) as total_amount_buy, users.full_name, users.email
FROM users
INNER JOIN orders
ON users.id = orders.user_id
GROUP BY users.full_name, users.email
ORDER BY total_amount_buy DESC
LIMIT 1
;

-- c) Người trả số tiền hàng lớn nhất
SELECT SUM(food.price) as total_pay, users.full_name, users.email
FROM users
INNER JOIN orders
ON users.id = orders.user_id
INNER JOIN food 
ON orders.food_id = food.id
GROUP BY users.full_name, users.email
ORDER BY total_pay DESC
LIMIT 1
;

-- 4) Tìm người dùng không hoạt động trong hệ thống

SELECT users.full_name, users.email, COUNT(orders.user_id) as total_order, COUNT(rate_res.user_id) as total_rate, COUNT(like_res.user_id) as total_like
FROM users
LEFT JOIN orders
ON users.id = orders.user_id
LEFT JOIN rate_res
ON users.id = rate_res.user_id
LEFT JOIN like_res
ON users.id = like_res.user_id
GROUP BY users.full_name, users.email
HAVING total_order = 0 && total_rate = 0 && total_like = 0
;

-- 5) Tính trung bình sub_food của 1 food

SELECT food.name, AVG(sub_food.price) as average_price_of_sub_food, SUM(sub_food.price) as total_price_of_sub_food
FROM food
INNER JOIN sub_food
ON food.id = sub_food.food_id
GROUP BY food.name
;
